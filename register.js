const url = 'https://server.exozy.me'
const form = document.querySelector('form')

form.addEventListener('submit', (e) => {
  e.preventDefault()
  
  const formData = new FormData()
  
  formData.append('type', 'registration')
  
  const names = document.getElementsByName('names')[0].value
  formData.append('names', names)
  
  const emails = document.getElementsByName('emails')[0].value
  formData.append('emails', emails)

  const username = document.getElementsByName('username')[0].value
  formData.append('username', username)
  
  const password = document.getElementsByName('password')[0].value
  formData.append('password', password)
  
  fetch(url, {
    method: 'POST',
    body: formData,
  }).then((response) => {
    alert(response.statusText)
  })
})
