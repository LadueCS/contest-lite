const url = 'https://server.exozy.me'
const form = document.querySelector('form')

form.addEventListener('submit', (e) => {
  e.preventDefault()
  
  const formData = new FormData()
  
  formData.append('type', 'submission')
  
  const username = document.getElementsByName('username')[0].value
  formData.append('username', username)
  
  const password = document.getElementsByName('password')[0].value
  formData.append('password', password)
  
  const contest = document.getElementsByName('contest')[0].value
  formData.append('contest', contest)
  
  const problem = document.getElementsByName('problem')[0].value
  formData.append('problem', problem)
  
  const file = document.querySelector('[type=file]').files[0]
  formData.append('file', file)
  
  fetch(url, {
    method: 'POST',
    body: formData,
  }).then((response) => {
    alert(response.statusText)
  })
})
