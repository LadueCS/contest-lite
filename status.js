const url = 'https://server.exozy.me'
const form = document.querySelector('form')

form.addEventListener('submit', (e) => {
  e.preventDefault()
  
  const formData = new FormData()
  
  formData.append('type', 'status')
  
  const username = document.getElementsByName('username')[0].value
  formData.append('username', username)
  
  const password = document.getElementsByName('password')[0].value
  formData.append('password', password)
      
  const contest = document.getElementsByName('contest')[0].value
  formData.append('contest', contest)
  
  fetch(url, {
    method: 'POST',
    body: formData,
  }).then(response => response.text()).then(data => {
    console.log(data)
    document.getElementsByName('status')[0].innerHTML = data
  })
})
